from function import *
from time import time
import CONSTANTE
from datetime import datetime

if __name__ == '__main__':
    get_time()
    price = create_random_price()
    replay = True
    display_game()
    start = time()
    display_price(price)
    i = 0
    while replay==True:
        priceP = ask_value()
        answer = verify_price(price, priceP)
        display_answer(answer)
        end=time()
        i += 1
        t = end - start
        z=0 if CONSTANTE.temps-t<=0 else CONSTANTE.temps-t
        if endgame_timer(z) == False:
            write_file(1, z, i)
            write_json(False,z,i)
            replay = restart()
            start = time()

            continue
        elif end_game(answer):
            write_file(0, z , i)
            write_json(True,z,i)
            display_price(price)
            replay = restart()
            start = time()

            continue
        display_time_left(z)
